"""
Author: Victor Faner

Description:  The following code scrapes submissions, comments, and
submitted links from a given subreddit and saves the scraped data to
.csv and .html files.  The subreddit, sorting algorithm, and the number
of submissions/posts to scrape are selected either through
command-line argument or user-input.
"""

import configparser
import os
import sys
import re
import requests
import praw
import pandas as pd
from datetime import datetime

CREDENTIALS = str(os.getcwd() + '/etc/credentials')
OUTDIR_SUBREDDIT = str(os.getcwd() + '/output/subreddit/')
OUTDIR_COMMENTS = str(os.getcwd() + '/output/comments/')
OUTDIR_AUTHOR = str(os.getcwd() + '/output/author/')
OUTDIR_HTML = str(os.getcwd() + '/output/html/')
C_LIMIT = None  # Limit the number of submission comments to scrape
PATTERN = re.compile('(reddit\.)'
                     '|(redd\.it)'
                     '|(imgur\.)'
                     '|(youtube\.)'
                     '|(gfycat\.)'
                     '|(vimeo\.)'
                     '|(streamable\.)')  # URLs to ignore
S_KEYS = ['id',
          'title',
          'created_utc',
          'score',
          'upvote_ratio',
          'ups',
          'downs',
          'gildings',
          'distinguished',
          'edited',
          'locked',
          'stickied',
          'hide_score',
          'pinned',
          'num_comments',
          'num_crossposts',
          'num_reports',
          'report_reasons',
          'permalink',
          'selftext',
          'stickied',
          'subreddit_id',
          'subreddit_name_prefixed',
          'subreddit_subscribers',
          'suggested_sort',
          'title',
          'url']
R_KEYS = ['name',
          'link_karma',
          'comment_karma',
          'has_subscribed',
          'has_verified_email',
          'verified',
          'is_mod',
          'is_employee',
          'is_gold']
C_KEYS = ['id',
          'link_id',
          'created_utc',
          'parent_id',
          'body',
          'score',
          'ups',
          'downs',
          'depth',
          'gildings',
          'num_reports',
          'stickied',
          'score_hidden',
          'gilded',
          'distinguished',
          'edited',
          'is_submitter',
          'permalink',
          'stickied',
          'submission_title',
          'subreddit_name_prefixed',
          'submission.title']


def scrape(model, keys):
    """
    For a given submission, comment, or redditor PRAW model, this
    function scrapes any available data matching the specified dict keys.
    Because PRAW models are lazy objects, some attributes may not always
    be available to scrape.

    :param model:  Submission, Comment, or Redditor object to scrape
    :param keys:  PRAW Model Object attributes to scrape
    :return:  dict object of scraped attributes
    """
    data_d = {}

    # Check model attributes and exit early if none found
    try:
        model_d = vars(model)
    except TypeError:
        return data_d

    for key in keys:
        try:
            val = model_d[key]
        except AttributeError:
            continue
        except KeyError:
            continue
        data_d[key] = val

    return data_d


def generate_csv(list_, outdir, name):
    """
    This function generates .csv files for scraped submissions or
    comments pages and saves them to the specified directory and file
    name.

    :param list_:  list of dict objects containing scraped data
    :param outdir:  Output directory to save .csv file
    :param name:   .csv file name
    """
    df = pd.DataFrame(list_)
    dt = datetime.now()
    csv_name = str(outdir
                   + name
                   + '_'
                   + dt.strftime('%Y%m%d%H%M%S')
                   + '.csv')
    df.to_csv(csv_name, index=False)


def generate_html(url, outdir, name):
    """
    This function generates .html files for any scraped link submissions
    and saves them to the specified directory and file name.

    :param url:  URL of the linked submission.
    :param name:  Name for the saved .html file
    """
    res = requests.get(url)

    try:
        res.raise_for_status()
        dt = datetime.now()
        f_name = str(outdir
                     + name
                     + '_'
                     + dt.strftime('%Y%m%d%H%M%S')
                     + '.html')
        
        with open(f_name, 'w+b') as f:
            for chunk in res.iter_content(100000):
                f.write(chunk)

        print(str('URL \"'
                  + url
                  + '\"'
                  + ' successfully saved to '
                  + OUTDIR_HTML))

    except Exception as exc:
        print('There was a problem requesting URL "' + url + '": %s' % (exc))


# Parse a pre-made config file for PRAW client connection credentials
config = configparser.ConfigParser()
config.read(CREDENTIALS)
reddit = praw.Reddit(client_id=config.get('CLIENT', 'CLIENT_ID'),
                     client_secret=config.get('CLIENT', 'CLIENT_SECRET'),
                     user_agent=config.get('CLIENT', 'USER_AGENT'))

# Set subreddit to be scraped via command-line argument or user input
try:
    sub = sys.argv[1]
except IndexError:
    sub = input('Which subreddit would you like to scrape? ')

sub = sub.lower()

# Set number of submissions to scrape
try:
    limit = sys.argv[2]
except IndexError:
    limit = input('How many posts would you like to scrape? ')

while True:
    try:
        limit = int(limit)
    except ValueError:
        limit = input('Invalid input received. Number of posts to scrape must '
                      'be an integer value. Please enter an integer value. ')
        continue

    if limit <= 0:
        limit = input('Invalid input received. Number of posts to scrape must '
                      'be greater than 0. Please enter an integer value '
                      'greater than 0. ')
        continue
    else:
        break

# Set sorting algorithm
try:
    sort = sys.argv[3]
except IndexError:
    sort = input('What sorting algorithm should be used? (Choose from hot, '
                 'new, rising, controversial, top, or gilded) ')

while True:
    try:
        sort = sort.lower()
    except AttributeError:
        sort = input('Invalid sorting algorithm selected. Please choose from '
                     'hot, new, rising, controversial, top, or gilded. ')
        continue

    if sort not in ('hot', 'new', 'rising', 'controversial', 'top'):
        sort = input('Invalid sorting algorithm selected. Please choose from '
                     'hot, new, rising, controversial, top, or gilded. ')
        continue
    else:
        break

# Set time filter for top and controversial sorts
time_filter = 'all'  # Default value needed for sub_sort dict

if sort == 'top' or sort == 'controversial':
    try:
        time_filter = sys.argv[4]
    except IndexError:
        time_filter = input('What time-frame would you like to sort by? (Can '
                            'be one of: all, day, hour, month, week, year) ')

    while True:
        try:
            time_filter = time_filter.lower()
        except AttributeError:
            time_filter = input('What time-frame would you like to sort by? '
                                '(Can be one of: all, day, hour, month, week, '
                                'year) ')
            continue

        if time_filter not in ('all', 'day', 'hour', 'month', 'week, year'):
            time_filter = input('Invalid input. Sort time frame must be one of '
                                'all, day, hour, month, week, year. ')
            continue
        else:
            break

# Connect to the specified subreddit and generate submissions
subreddit = reddit.subreddit(sub)
sub_sort = {'hot': subreddit.hot(limit=limit),
            'new': subreddit.new(limit=limit),
            'rising': subreddit.rising(limit=limit),
            'gilded': subreddit.gilded(limit=limit),
            'top': subreddit.top(time_filter=time_filter, limit=limit),
            'controversial': subreddit.controversial(time_filter=time_filter,
                                                     limit=limit)}
submissions = sub_sort[sort]

# Loop through and scrape all submissions, submission authors,
# submission comments pages, and comment authors
s_list = []
c_list = []
s_count = 0  # Use for list indexing to append redditor key-values to
             # dicts

for submission in submissions:
    s_list.append(scrape(submission, S_KEYS))
    redditor = submission.author
    s_list[s_count].update(scrape(redditor, R_KEYS))

    # Check regex for urls to ignore before requesting html files
    regex = PATTERN.search(submission.url.lower())
    if regex is None:
        generate_html(submission.url,
                      OUTDIR_HTML,
                      str(submission.id + sub + sort))

    # Generate and loop through comments page for the current submission
    submission.comments.replace_more(limit=C_LIMIT)
    c_count = 0  # Use for list indexing to append redditor key-values to
                 # dicts
    for comment in submission.comments.list():
        c_list.append(scrape(comment, C_KEYS))
        redditor = comment.author
        c_list[c_count].update(scrape(redditor, R_KEYS))
        c_count += 1

    generate_csv(c_list, OUTDIR_COMMENTS, str(submission.id
                                              + '_'
                                              + subreddit.display_name
                                              + '_'
                                              + sort))
    print('Comments for submission "'
          + submission.id
          + '" successfully scraped and saved to '
          + OUTDIR_COMMENTS)

    s_count += 1

# Save out submission data for all scraped posts
generate_csv(s_list, OUTDIR_SUBREDDIT, str(subreddit.display_name + '_' + sort))
print(str(limit)
      + ' '
      + sort
      + ' submissions from r/'
      + sub
      + ' successfully scraped and saved to '
      + OUTDIR_SUBREDDIT)
